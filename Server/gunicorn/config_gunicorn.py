"""gunicorn WSGI server configuration."""
#https://docs.gunicorn.org/en/latest/configure.html
#https://docs.gunicorn.org/en/latest/settings.html#settings

from multiprocessing import cpu_count
from os import environ


def max_workers():
    return cpu_count()


# bind = '0.0.0.0:' + environ.get('PORT', '8000')
max_requests = 5 #1000
# worker_class = 'gevent'
workers = max_workers()
timeout = 30
