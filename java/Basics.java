import java.util.Scanner;


public class Basics {

    public static void main(String[] args){
        int studentAge = 15;
        double studentGPA = 3.45;

        boolean hasPerfectAttendance = true;
        String studentFirstName = "Kayla";
        String studentLastName = "Hammon";
        char studentFirstInitial = studentFirstName.charAt(0);
        char studentLastInitial = studentLastName.charAt(0);

        Scanner input = new Scanner(System.in);

        // System.out.println(studentAge);
        System.out.println(studentFirstName + " " + studentLastName + " Has a GPA of " + studentGPA);
        System.out.println("What do you want to change it to?");
        studentGPA = input.nextDouble();
        System.out.println("GPA updated: " + studentGPA);

        // System.out.println(studentLastInitial);
        // System.out.println(studentFirstInitial);
        // System.out.println(hasPerfectAttendance);
        // System.out.println(studentFirstName);
        // System.out.println(studentLastName);


    }





}
