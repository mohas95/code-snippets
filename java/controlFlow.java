import java.util.Scanner;

public class controlFlow{

    public static void main(String[] args) {
        // System.out.println("Pick a number between 1 and 10");
        Scanner scanner = new Scanner(System.in);

        /*  If condition */

        //
        // int inputtedNum = scanner.nextInt();
        //
        // if (inputtedNum <5) {
        //     System.out.println("Enjoy the goodluck a friend brings you");
        // }else{
        //     System.out.println("Your shoe selection will make you happy today");
        // }


        /*  While loop */

        boolean isOnRepeat = true;

        while(isOnRepeat){
            System.out.println("playing current song");
            System.out.println("would you like to take this song off of repeat? If so, answer yes");

            String userInput = scanner.next();

            if(userInput.equals("yes")){
                isOnRepeat = false;
            }
        }

        System.out.println("Song is no longer on repeat, exiting program");

    }


}
