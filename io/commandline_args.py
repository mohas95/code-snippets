###############################
# get commandline arguements  #
###############################

import argparse

def get_args():
    parser = argparse.ArgumentParser()

    parser.add_argument('content_image_path', type = str, help = 'path to the content image you want a style applied to')
    parser.add_argument('style_image_path', type = str, help = 'path to the style image')
    parser.add_argument('output_image_path', type = str, help = 'path to save the output path')

    return parser.parse_args()
