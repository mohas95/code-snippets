import numpy as np
import matplotlib.pyplot as plt
import cv2
from load_image_module import load_image



image_copy, image_rgb, image_gray = load_image('./assets/thumbs_up_down.jpg')

plt.figure()
plt.imshow(image_gray, cmap = 'gray')

retval, binary = cv2.threshold(image_gray, 225,255, cv2.THRESH_BINARY_INV)

plt.figure()
plt.imshow(binary, cmap='gray')


contours, hierarchy = cv2.findContours(binary, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

all_contours = cv2.drawContours(image_copy, contours, -1, (0,255,0), 2)

plt.figure()
plt.imshow(all_contours)


plt.show()
