import numpy as np
import matplotlib.pyplot as plt
import cv2
from load_image_module import load_image



image_copy, rgb_image, gray_image = load_image('../assets/phone.jpg')

# plt.figure()
# plt.imshow(rgb_image)
# plt.figure()
# plt.imshow(gray_image, cmap = 'gray')



low_threshold = 50
high_threshold = 100
edges = cv2.Canny(gray_image, low_threshold, high_threshold)


plt.figure()
plt.imshow(edges, cmap = 'gray')


rho = 1
theta = np.pi/180
threshold = 60
min_line_length = 100
max_line_gap = 5

lines = cv2.HoughLinesP(edges, rho, theta, threshold, np.array([]), min_line_length, max_line_gap)

line_image = np.copy(image_copy)

for line in lines:
    for x1,y1,x2,y2 in line:
        cv2.line(line_image, (x1,y1),(x2,y2), (255, 0, 0),5)

plt.figure()
plt.imshow(line_image)





# # for drawing circles on
# circles_im = np.copy(image)
#
# # right now there are too many, large circles being detected
# # try changing the value of maxRadius, minRadius, and minDist
# circles = cv2.HoughCircles(gray_blur, cv2.HOUGH_GRADIENT, 1,
#                            minDist=45,
#                            param1=70,
#                            param2=11,
#                            minRadius=20,
#                            maxRadius=40)
#
# # convert circles into expected type
# circles = np.uint16(np.around(circles))
# # draw each one
# for i in circles[0,:]:
#     # draw the outer circle
#     cv2.circle(circles_im,(i[0],i[1]),i[2],(0,255,0),2)
#     # draw the center of the circle
#     cv2.circle(circles_im,(i[0],i[1]),2,(0,0,255),3)
#
# plt.imshow(circles_im)






plt.show()
