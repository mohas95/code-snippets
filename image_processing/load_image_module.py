import numpy as np
import matplotlib.pyplot as plt
import cv2


def load_image(image_path):
    ''' input args: path to image that needs to be loaded
    ouput: np array of rgb version of image, and gray scale version of image '''

    image = cv2.imread(image_path)

    image_copy = np.copy(image)
    rgb_image = cv2.cvtColor(image_copy, cv2.COLOR_BGR2RGB)

    gray_image = cv2.cvtColor(rgb_image, cv2.COLOR_RGB2GRAY)

    return image_copy,rgb_image, gray_image


if __name__ == '__main__':


    rgb_pizza, gray_pizza = load_image('./assets/pizza_bluescreen.jpg')

    plt.figure()
    plt.imshow(rgb_pizza)

    plt.figure()
    plt.imshow(gray_pizza, cmap = 'gray')

    plt.show()
