import numpy as np
import matplotlib.pyplot as plt
import cv2

image_path = '../assets/water_balloons.jpg'

image = cv2.imread(image_path)
image_copy = np.copy(image)
image_copy = cv2.cvtColor(image_copy, cv2.COLOR_BGR2RGB) #cv2 always imports as BGR over RGB so we conver

plt.figure()
plt.imshow(image_copy)

# RGB Color channels

r = image_copy[:,:,0]
g = image_copy[:,:,1]
b = image_copy[:,:,2]

lower_pink = np.array([180,0,100])
upper_pink = np.array([255,255,230])

mask_rgb = cv2.inRange(image_copy, lower_pink, upper_pink)
masked_rgb_image = np.copy(image_copy)
masked_rgb_image[mask_rgb == 0] = [0,0,0]

f, (a1,a2,a3) = plt.subplots(1,3,figsize=(20,10))

a1.set_title('Red')
a1.imshow(r, cmap='gray')

a2.set_title('Green')
a2.imshow(g, cmap='gray')

a3.set_title('Blue')
a3.imshow(b, cmap='gray')






# HSV Color channels

hsv = cv2.cvtColor(image_copy, cv2.COLOR_RGB2HSV)

h = hsv[:,:,0]
s = hsv[:,:,1]
v = hsv[:,:,2]

lower_hue = np.array([160,0,0])
upper_hue = np.array([180,255,255])
mask_hsv = cv2.inRange(hsv, lower_hue, upper_hue)

masked_hsv_image= np.copy(image_copy)
masked_hsv_image[mask_hsv==0] = [0,0,0]


f, (a1,a2,a3) = plt.subplots(1,3,figsize=(20,10))

a1.set_title('Hue')
a1.imshow(h, cmap='gray')

a2.set_title('Saturation')
a2.imshow(s, cmap='gray')

a3.set_title('Value')
a3.imshow(v, cmap='gray')

## plot the masked
f, (a1,a2) = plt.subplots(1,2,figsize=(20,10))

a1.set_title('Pink')
a1.imshow(masked_rgb_image)

a2.set_title('Hue')
a2.imshow(masked_hsv_image)

plt.show()
