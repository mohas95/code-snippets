import numpy as np
import matplotlib.pyplot as plt
import cv2


image_path = 'assets/water_balloons.jpg'
image = cv2.imread(image_path)
image_copy = np.copy(image)

image_copy = cv2.cvtColor(image_copy, cv2.COLOR_BGR2RGB)

plt.figure()
plt.imshow(image_copy)


gray = cv2.cvtColor(image_copy, cv2.COLOR_RGB2GRAY)

plt.figure()
plt.imshow(gray, cmap = 'gray')


lower = 120
upper = 240

edges = cv2.Canny(gray, lower, upper)

plt.figure()
plt.imshow(edges, cmap = 'gray')


plt.show()
