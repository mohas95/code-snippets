def punctuation_token_lookup():
    """
    Generate a dict to turn punctuation into a token.
    :return: Tokenized dictionary where the key is the punctuation and the value is the token
    """
    # TODO: Implement Function
    punctuation_dict ={'.':'||Period||', ',':'||Comma||','"':'||Quotation_Mark||',';':'||Semicolon||','!':'||Exclamation_Mark||',
                       '?':'||Question_Mark||','(':'||Left_Parentheses||',')':'||Right_Parentheses||','-':'||Dash||','\n':'||Retun||'}


    return punctuation_dict
