import numpy as np
from torch.utils.data import TensorDataset, DataLoader


def batch_data(words, sequence_length, batch_size):
    """
    Batch the neural network data using DataLoader
    :param words: The word ids of the TV scripts
    :param sequence_length: The sequence length of each batch
    :param batch_size: The size of each batch; the number of sequences in a batch
    :return: DataLoader with batched data
    """

    target_idx = sequence_length  + 1

    ## Cleans up un even word data by removing any remainder words that dont fit sequence length
    if len(words)%target_idx != 0:

        num_words_to_pop = len(words)%target_idx

        words = words[:-num_words_to_pop]

    # Reshapes and process the input words and target for RNN (wich is the the next word in the sequence)
    words = np.array(words).reshape((-1,target_idx))

    input_data = words[:,:sequence_length]
    targets = words[:,sequence_length].reshape((words.shape[0],1))

#     =====testing===== uncomment below to test processing
#     print(input_data)
#     print(targets)

    #loads the data to pytorchloader
    data = TensorDataset(torch.from_numpy(input_data), torch.from_numpy(targets))
    data_loader = DataLoader(data, batch_size=batch_size)



    # return a dataloader
    return data_loader
